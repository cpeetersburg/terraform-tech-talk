# Module definitions
variable "resource_group_name" {
  type = string
}
variable "location_name" {
  type = string
}
variable "environment" {
  type = string
}
variable "project_name" {
  type = string
}

# Define a resource group
resource "azurerm_resource_group" "resource_group" {
  name     = var.resource_group_name
  location = var.location_name

  tags = {
    "environment" = var.environment
    "project"     = var.project_name
  }
}

# Create a public IP address
resource "azurerm_public_ip" "public_ip" {
  name                = "pip-${var.environment}-${var.location_name}-001"
  resource_group_name = azurerm_resource_group.resource_group.name
  location            = var.location_name
  allocation_method   = "Static"

  tags = {
    "environment" = var.environment
    "project"     = var.project_name
  }
}

# Create a virtual network
resource "azurerm_virtual_network" "virtual_network" {
  name                = "vnet-${var.environment}-${var.location_name}-001"
  address_space       = ["10.0.0.0/16"]
  location            = var.location_name
  resource_group_name = azurerm_resource_group.resource_group.name
}

# Create a subnet
resource "azurerm_subnet" "subnet" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.resource_group.name
  virtual_network_name = azurerm_virtual_network.virtual_network.name
  address_prefixes     = ["10.0.0.0/24"]
}

# Create a network interface
resource "azurerm_network_interface" "network_interface" {
  name                = "nic-${var.environment}-${var.location_name}-001"
  location            = var.location_name
  resource_group_name = azurerm_resource_group.resource_group.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet.id
    public_ip_address_id          = azurerm_public_ip.public_ip.id
    private_ip_address_allocation = "Dynamic"
  }
}

# Create a virtual machine
resource "azurerm_virtual_machine" "virtual_machine" {
  name                  = "vm-${var.environment}-${var.location_name}-001"
  location              = var.location_name
  resource_group_name   = azurerm_resource_group.resource_group.name
  network_interface_ids = [azurerm_network_interface.network_interface.id, ]
  vm_size               = "Standard_A4_v2"

  storage_os_disk {
    name              = "vm-${var.environment}-${var.location_name}-001-os"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    publisher = "canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts"
    version   = "latest"
  }

  os_profile {
    computer_name  = "server-${var.environment}-${var.location_name}-001"
    admin_username = "webadmin"
    admin_password = "@p4ch3P@$$w0rd"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags = {
    environment = var.environment
    project     = var.project_name
  }

  provisioner "file" {
    connection {
      type = "ssh"
      host = azurerm_public_ip.public_ip.ip_address
      user = "webadmin"
      password = "@p4ch3P@$$w0rd"
    }

    source      = "bootstrapweb.sh"
    destination = "/tmp/bootstrap.sh"
  }

  provisioner "remote-exec" {
    connection {
      type = "ssh"
      host = azurerm_public_ip.public_ip.ip_address
      user = "webadmin"
      password = "@p4ch3P@$$w0rd"
    }

    inline = [
      "chmod +x /tmp/bootstrap.sh",
      "/tmp/bootstrap.sh"
    ]
  }
}