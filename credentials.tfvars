################################################################################
#                             Terraform Tech Talk                              #
#                             powered by Lecroq BV                             #
################################################################################

# The credentials (credentials.tfvars) file
# Author: Christopher Peeters <cp@linux.com>
# Date: 2021-07-27

# Azure
azure_subscription_id = ""
azure_client_id       = ""
azure_client_secret   = ""
azure_tenant_id       = ""