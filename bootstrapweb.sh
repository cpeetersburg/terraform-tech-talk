#!/usr/bin/env bash
################################################################################
#                             Terraform Tech Talk                              #
#                             powered by Lecroq BV                             #
################################################################################

# The bootstrap script (bootstrapweb.sh) file
# Author: Christopher Peeters <cp@linux.com>
# Date: 2021-07-27

sudo apt update && sudo apt upgrade -y
sudo apt install -y apache2

cat <<EOF | sudo tee /var/www/html/index.html
<html>
<head>
<title>Web server set up by Terraform on Azure</title>
</head>
<body>
<h2>Welcome on the Terraform web server!</h1>
<p>Web server set up by Terraform on Azure.</p>
<p>ITGilde Tech Talk: Terraform by Christopher Peeters</p>
</body>
</html>
EOF

sudo chown -R www-data:www-data /var/www/html
sudo systemctl enable --now apache2