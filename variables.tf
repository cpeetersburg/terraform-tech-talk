################################################################################
#                             Terraform Tech Talk                              #
#                             powered by Lecroq BV                             #
################################################################################

# The variables (variables.tf) file
# Author: Christopher Peeters <cp@linux.com>
# Date: 2021-07-27

# Variable definitions
variable "azure_subscription_id" {}
variable "azure_client_id" {}
variable "azure_client_secret" {}
variable "azure_tenant_id" {}

variable "resource_group_name" {
  type        = string
  description = "The name of the Azure Resource Group."
}

variable "location_name" {
  type        = string
  description = "The name of the Azure Region."
}

variable "environment" {
  type        = string
  description = "The environment for which the component is deployed."
}

variable "project_name" {
  type        = string
  description = "The name of the project for which the infra is used."
}