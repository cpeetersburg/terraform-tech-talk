################################################################################
#                             Terraform Tech Talk                              #
#                             powered by Lecroq BV                             #
################################################################################

# The project variables (project.tfvars) file
# Author: Christopher Peeters <cp@linux.com>
# Date: 2021-07-27

resource_group_name = "rg-webserver-dev-euw1-001"
location_name       = "southafricanorth"
environment         = "dev"
project_name        = "Terraform Tech Talk"