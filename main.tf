################################################################################
#                             Terraform Tech Talk                              #
#                             powered by Lecroq BV                             #
################################################################################

# The main (main.tf) file
# Author: Christopher Peeters <cp@linux.com>
# Date: 2021-07-27

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 2.56"
    }
  }

  required_version = ">= 0.14.10"
}

provider "azurerm" {
  # Authentication
  use_msi         = true
  subscription_id = var.azure_subscription_id
  client_id       = var.azure_client_id
  client_secret   = var.azure_client_secret
  tenant_id       = var.azure_tenant_id

  features {}
}

# Create a virtual machine
module "virtual_machine" {
  source              = "./modules/virtual-machine"
  resource_group_name = var.resource_group_name
  location_name       = var.location_name
  environment         = var.environment
  project_name        = var.project_name
}